import os
from google.cloud.sql.connector import Connector, IPTypes

# Set your service account credentials file
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = ".\speedy-center-key.json"
# print(os.environ["GOOGLE_APPLICATION_CREDENTIALS"])
# Cloud SQL instance connection name
INSTANCE_CONNECTION_NAME = "speedy-center-420103:northamerica-northeast2:sampletictactoe"

# Database details
DB_USER = "root"
DB_PASSWORD = "12345"
DB_NAME = "tictactoedb"

# Initialize Connector
connector = Connector()


# Function to return a connection object
def get_connection():
    conn = connector.connect(
        INSTANCE_CONNECTION_NAME,
        "pymysql",
        user=DB_USER,
        password=DB_PASSWORD,
        db=DB_NAME,
        ip_type=IPTypes.PUBLIC,  # Use PRIVATE if using a private IP
    )
    return conn


# Example usage
try:
    conn = get_connection()
    cursor = conn.cursor()

    # Example query
    cursor.execute("SHOW TABLES")
    tables = cursor.fetchall()

    print("Tables:", tables)

    # Close cursor and connection
    cursor.close()
    conn.close()

except Exception as e:
    print(f"Error: {e}")

# Close the Connector
connector.close()
